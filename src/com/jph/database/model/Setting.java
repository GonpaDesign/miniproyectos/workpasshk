package com.jph.database.model;

import java.time.LocalDate;
import java.util.Date;


/**
* Getter y Setter de la clase la entidad Setting
* @author Gregorio Bolivar
* dni,fichada,created_at,device
*/
public class Setting {
   static String id;
   static String dni;
   static String fichada;
   static LocalDate created_at;
   static String device;


   public static void setId(String id) {
	   Setting.id = id;
   }
   
   public static String getId() {
       return id;
   }
   
   public static void setDni(String dni) {
	   Setting.dni = dni;
   }

   public static String getDni() {
       return dni.trim();
   }
   
   public static void setFichada(String fichada) {
	   Setting.fichada = fichada;
   }

   /**
    * Retornar fichada setteada
    */
   public static String getFichada() {
       return fichada;
   }

   public static void setCreated(LocalDate date) {
	   Setting.created_at = date;
   }
   

   public static LocalDate getCreated() {
       return created_at;
   }

   public static void setDevice(String device) {
	   Setting.device = device;
   } 


   public static String getDevice() {
       return device;
   }

  
   
}
