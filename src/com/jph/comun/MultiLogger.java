package com.jph.comun;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class MultiLogger  {

	
		/**
		* Metodo interno que realmente hace el Logueo
		*
		* @param c - Clase que hizo la llamada orginalmente 
		* @param l - Nivel de Logueo
		* @param message - Mensaje de Logueo 
		* @param e - Excepcion si este es un Logueo de excepcion
		**/
		private static void logMessage(Class c,Level l,String message,Throwable e){
			Logger logger = null;
			
			if (c == null)
				logger = LogManager.getRootLogger();
			else
				logger = LogManager.getLogger(c);
		
			if (e == null){
				logger.log(l,message);
			}else{
				logger.log(l,message,e);
			}
		}
		
		
		/**
		* Logueo de un mensaje con nivel FATAL 
		*
		* @param c - Clase que origina el logueo 
		* @param message Mensaje a Loguear
		* @param e - Excepci�n que ocurri� y se quiere loguear
		**/
		public static void fatalMessage(Class c,String message,Throwable e){
			logMessage(c,Level.FATAL,message,e);
		}
		
		/**
		* Logueo de un mensaje con nivel FATAL 
		*
		* @param c - Clase que origina el logueo 
		* @param message Mensaje a Loguear
		**/
		public static void fatalMessage(Class c,String message){
			logMessage(c,Level.FATAL,message,null);
		}					
		
		/**
		* Logueo de un mensaje con nivel FATAL 
		*
		* @param message Mensaje a Loguear
		* @param e Excepci�n que ocurri� y se quiere loguear
		**/
		public static void fatalMessage(String message,Throwable e){
			logMessage(null,Level.FATAL,message,e);
		}	
		
		/**
		* Logueo de un mensaje con nivel FATAL 
		*
		* @param message Mensaje a Loguear
		**/
		public static void fatalMessage(String message){
			logMessage(null,Level.FATAL,message,null);
		}
		  
		/**
		* Logueo de un mensaje con nivel ERROR 
		*
		* @param c - Clase que hizo la llamada orginalmente 
		* @param message Mensaje a Loguear
		* @param e Excepci�n que ocurri� y se quiere loguear
		**/
		public static void errorMessage(Class c,String message,Throwable e){
			logMessage(c,Level.ERROR,message,e);
		}		

		/**
		* Logueo de un mensaje con nivel ERROR 
		*
		* @param c - Clase que origina el logueo 
		* @param message Mensaje a Loguear
		**/
		public static void errorMessage(Class c,String message){
			logMessage(c,Level.ERROR,message,null);
		}

		/**
		* Logueo de un mensaje con nivel ERROR
		* 
		* @param message Mensaje a Loguear
		*/
		public static void errorMessage(String message){
			logMessage(null,Level.ERROR,message,null);
		}	
		  
		/**
		 * Logueo de un mensaje con nivel ERROR
		 * 
		 * @param message Mensaje a Loguear
		 * @param e Excepci�n que ocurri� y se quiere loguear
		*/
		public static void errorMessage(String message, Throwable e){
			logMessage(null,Level.ERROR,message,e);
		}	
		  
		  
		/**
		* Logueo de un mensaje con nivel WARNING 
		*
		* @param c Clase que origina el logueo
		* @param message Mensaje a Loguear
		* @param e Excepci�n que ocurri� y se quiere loguear
		**/
		public static void warnMessage(Class c,String message,Throwable e){
			 logMessage(c,Level.WARN,message,e);
		}	

		/**
		*  
		*
		* @param c Clase que origina el logueo
		* @param message Mensaje a Loguear
		**/
		public static void warnMessage(Class c,String message){
			logMessage(c,Level.WARN,message,null);
		}
		
		/**
		 * Logueo de un mensaje con nivel WARNING
		 * 
		 * @param message Mensaje a Loguear
		 */
		public static void warnMessage(String message){
			logMessage(null,Level.WARN,message,null);
		}
		
		/**
		 * Logueo de un mensaje con nivel WARNING
		 * 
		 * @param message Mensaje a Loguear
		 * @param e Excepci�n que ocurri� y se quiere loguear
		 */
		public static void warnMessage(String message, Throwable e){
			logMessage(null,Level.WARN,message,e);
		}
		

		/**
		* Logueo de un mensaje con nivel INFO 
		*
		* @param c Clase que origina el logueo
		* @param message Mensaje a Loguear
		**/
		public static void infoMessage(Class c,String message){
			logMessage(c,Level.INFO,message,null);
		}

		/**
		* Logueo de un mensaje con nivel INFO 
		*
		* @param c - Clase que origina el logueo
		* @param message Mensaje a Loguear
		* @param e Excepci�n que ocurri� y se quiere loguear
		**/
		public static void infoMessage(Class c,String message,Throwable e){
			logMessage(c,Level.INFO,message,e);
		}	

		/**
		* Logueo de un mensaje con nivel DEBUG 
		*
		* @param c Clase que origina el logueo 
		* @param message Mensaje a Loguear
		* @param e Excepci�n que ocurri� y se quiere loguear
		**/
		public static void debugMessage(Class c,String message,Throwable e){
			logMessage(c,Level.DEBUG,message,e);
		}		


		/**
		* Logueo de un mensaje con nivel DEBUG 
		*
		* @param c Clase que origina el logueo 
		* @param message Mensaje a Loguear
		**/
		public static void debugMessage(Class c,String message){
			logMessage(c,Level.DEBUG,message,null);
		}					// debugMessage
		

		/**
		 * Logueo de un mensaje con nivel DEBUG
		 *   
		 * @param message Mensaje a Loguear
		 * @param e Excepci�n que ocurri� y se quiere loguear
		 */
		public static void debugMessage(String message,Throwable e){
			logMessage(null,Level.DEBUG,message,e);
		}	

		/**
		 * Logueo de un mensaje con nivel DEBUG
		 * 
		 * @param message Mensaje a Loguear
		 */
		public static void debugMessage(String message){
			logMessage(null,Level.DEBUG,message,null);
		}	
}