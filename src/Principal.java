import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.jph.businesslogic.BusinessLogic;
import com.jph.clientws.ClienteJson;
import com.jph.comun.MultiLogger;
import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;
import com.pi4j.util.Console;
/*
import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;
*/

public class Principal {
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	// create an instance of the serial communications class
    final static Serial serial = SerialFactory.createInstance();
    static String text ="";
    
	public static void main(String[] args) throws InterruptedException {
		final Serial serial = SerialFactory.createInstance();
		final Console console = new Console();
		MultiLogger.debugMessage(ClienteJson.class,"Iniciando Java");
        // print program title/header
        console.title("Serial Communication");
        // allow for user to exit program using CTRL-C
        console.promptForExit();
        // create and register the serial data listener
        serial.addListener(new SerialDataEventListener() {
            @Override
            public void dataReceived(SerialDataEvent event) {
                // print out the data received to the console
                try {
                	// Todo el string que devuele el serial
                	String eventLog = event.getAsciiString();
                	text += eventLog;
                    System.out.print(eventLog);
                    if(text.indexOf('\n') != -1){
                    	MultiLogger.debugMessage(ClienteJson.class,"String recibido en salto de linea: " + text);
	                	boolean resultado = text.contains("VERIFIED");
	                	if(resultado && text.indexOf(')') != -1){
	                    	System.out.println("Proceso verificado");
	                    	System.out.println(text);
	                    	MultiLogger.debugMessage(ClienteJson.class,"String recibido: " + text);
	                    	String dni = text.substring(text.indexOf('(')+1, text.indexOf(')'));	                    	
	                    	System.out.println("encontrado DNI: " + dni);
	                			// Mandamos el evento del wigand a la logica del negocio
                            BusinessLogic logic = new BusinessLogic();
                            logic.getDocumentBySerial(dni);
	                	}
	                	text = "";
                    }
                } catch (Exception e) {
                	text = "";
                	MultiLogger.debugMessage(ClienteJson.class,"Excepcion en Principal Listener ", e);
                    //e.printStackTrace();
                }
            }
        });

        try {
            // create serial config object
            SerialConfig config = new SerialConfig();
            config.device("/dev/ttyAMA0")
            //config.device(SerialPort.getDefaultPort())
                  .baud(Baud._19200)
                  .dataBits(DataBits._8)
                  .parity(Parity.NONE)
                  .stopBits(StopBits._1)
                  .flowControl(FlowControl.NONE);
            // display connection details
            console.box(" Connecting to: " + config.toString());
            // open the default serial device/port with the configuration settings
            serial.open(config);
            // continuous loop to keep the program running until the user terminates the program
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));                     
            while(true) {
            	try {
                    String s = br.readLine();
                    serial.writeln(s);
                }
                catch(IllegalStateException ex){
                    ex.printStackTrace();
                }
            }
        }catch(IOException ex) {
            console.println(" ==>> SERIAL SETUP FAILED : " + ex.getMessage());
            return;
        }
	}
}
