package com.jph.database.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.jph.database.conn.Conexion;

public class Operaciones extends Conexion{
    /**
     * Constructor for objects of class Operaciones
     */
    public Operaciones(){}
    
    public int insertar(String sql){
    	ResultSet valor = null;
        conectar();
        int V2 = 0;
		try {
            consulta.executeUpdate(sql);
        } catch (SQLException e) {
                System.out.println("Mensaje:"+e.getMessage());
            }      
        finally{  
            try{  
            	valor = consulta.getGeneratedKeys();
            	valor.next();
            	V2 = valor.getInt(1);
    			System.out.println(valor.getInt(1));
                 consulta.close();  
                 conexion.close();  
             }catch (Exception e){                 
                 e.printStackTrace();  
             }  
        }

        return V2;
    }
    
    public ResultSet consultar(String sql){
        conectar();
        ResultSet resultado = null;
        try {
            resultado = consulta.executeQuery(sql);

        } catch (SQLException e) {
                System.out.println("Mensaje:"+e.getMessage());
                System.out.println("Estado:"+e.getSQLState());
                System.out.println("Codigo del error:"+e.getErrorCode());
                
            }
        return resultado;
    }
    
  
    
    
}