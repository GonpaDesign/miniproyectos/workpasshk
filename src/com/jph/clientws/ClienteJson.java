package com.jph.clientws;

/**
 * Importaciones de los datos de java para api rest 
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

import com.jph.comun.MultiLogger;
import com.jph.comun.Propiedades;

public class ClienteJson{
    //private static final String PrintStream = null;
    private static String dni = "";
    
	public ClienteJson(String Dni) { 	
    	ClienteJson.dni = Dni;
	}

	// SAAJ - SOAP Client Testing
    public void start() throws UnsupportedEncodingException {
    	System.out.println("Dato capturado del valor de documento :"+dni);
        callJsonWebService();
    }


    private static void callJsonWebService() {
    	URL url;
    	try {
    		// PArametros de los datos de entrada para procesar el api
    		String param1 = Propiedades.leer("param1");
            String param2 = Propiedades.leer("param2");
            String hbl = Propiedades.leer("HBL");
            
            String jsonEndpointUrl = Propiedades.leer("jsonEndpointUrl")+"?"+param1+"="+hbl+"&"+param2+"="+dni.trim();
            //String jsonAction = Propiedades.leer("jsonAction");
            
            System.out.println("Ruta formada para enviar la peticion: "+jsonEndpointUrl);
            
            MultiLogger.debugMessage(ClienteJson.class, "Enviando datos al api:  "+jsonEndpointUrl);
            
             url = new URL(jsonEndpointUrl);
            Map<String,Object> params = new LinkedHashMap<>();
            params.put(param1, hbl);
            params.put(param2, dni.trim());
            
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            
            System.out.println(params.get(0));
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            
            conn.getOutputStream().write(postDataBytes);

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            for (int c1; (c1 = in.read()) >= 0;)
                System.out.print((char)c1);
            
        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
        }
    }



}