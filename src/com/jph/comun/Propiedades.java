package com.jph.comun;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class Propiedades {
                private static Properties prop = null;
                
                private static  Properties loadProperty(){
                	 
                               InputStream entrada = null;
                               try{
                                    if (prop == null){
                                          entrada = new FileInputStream("/home/pi/Documents/workpass/config.properties");
                                         
                                          prop= new Properties();
                                          prop.load(entrada);
                                     }

                               } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                               return prop;
                }

               

                public static final String leer(String propiedad){
                   String valor = "";
                   try{
                       valor = loadProperty().getProperty(propiedad);                                 

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                       return valor;
                }
                
                public static final String leerDevice(String device,String propiedad){
                    String valor = "";
                    try{
                        valor = loadProperty().getProperty(device + "_" + propiedad);                                 

                    } catch (Exception ex) {
                    	ex.printStackTrace();
                    }

                    return valor;
                }

               

                public static void main(String[] args) {
                               System.out.println(Propiedades.leer("url_n4_wsdl"));
                               System.out.println(Propiedades.leer("url_n4_argo"));
                               System.out.println(Propiedades.leer("url_billing"));
                               System.out.println(Propiedades.leer("url_agp"));
                               System.out.println(Propiedades.leer("url_icbc"));
                }

}
