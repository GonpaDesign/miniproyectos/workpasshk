package com.jph.businesslogic;

import java.io.UnsupportedEncodingException;
import com.jph.clientws.ClienteJson;


public class BusinessLogic {
	/**
	 * Permite recibir el valor del wigand para procesar la logica del negocio
	 * @param valor
	 */
	public void getDocumentBySerial(String dni){
		System.out.println(dni);
		// Consumir el servicio soap WS de TRP
		ClienteJson clientejson = new ClienteJson(dni);
		
		try {
			clientejson.start();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
