package com.jph.database.conn;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

import com.jph.comun.Propiedades;
/**
 * Write a description of class Conexion here.
 * 
 **/
public class Conexion{
	protected Connection conexion;
	protected Statement consulta;
	public String ruta;

    /**
     * Constructor for objects of class Conexion
     */
    public Conexion()
    {
    	String ruta = Propiedades.leer("ruta");
    	System.out.println(ruta);
    }
    public void conectar(){
		try {
	            Class.forName("org.sqlite.JDBC");
	        }
	        catch (ClassNotFoundException e) {
	            JOptionPane.showMessageDialog(null, e.getMessage());
	        }	 
			try {
                conexion = DriverManager.getConnection("jdbc:sqlite:"+ruta);
                consulta = conexion.createStatement();
                System.out.println("Connection Successful.");
			} catch (SQLException e) {
               JOptionPane.showMessageDialog(null, e.getMessage());
            }
	}
    
}
